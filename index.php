<?php
// require('kendaraan.php');
require('animal.php');
require('ape.php');
require('frog.php');

$sheep = new Animal("shaun");

echo $sheep->get_name();
echo $sheep->get_legs();
echo $sheep->get_cold_blooded();
// var_dump($sheep);

echo "<br>";

$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"
// var_dump($sungokong);

echo "<br>";

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"
// var_dump($kodok);


?>